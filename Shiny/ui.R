library(shiny)
library(shinythemes) #dodamo nove teme
library(tidyr)
library(leaflet)


shinyUI(fluidPage(
  theme = shinytheme("journal"), #izbira teme
  tags$head(
    tags$style(HTML("
                   @import url(https://fonts.googleapis.com/css?family=Roboto); #uvozimo pisavo Roboto
                    "))
    ),
  titlePanel("Dog Breeds",
             tags$head(
               tags$img(src="group_of_dogs.gif")  #slika v headerju
               #tags$img(src="paw.png", height="120px"),
               #tags$img(src="paw.png", height="90px"),
               #tags$img(src="paw.png", height="110px")
             )),
  
  sidebarLayout(
    sidebarPanel(
      list(tags$head(
        #tags$style(type='text/css', ".table { font-family: 'Roboto', sans-serif;
        #               font-weight: 400; line-height: 1.1; font-size: 12pt; }"),
        tags$style(type="text/css", "body {background-color: #FFFAF0;}"),
        tags$style(type="text/css","#breedinfo2 table {background-color: #FFE0A3; }", media="screen"),
        tags$style(type="text/css","#breedinfo1 table {background-color: #FFE0A3; }", media="screen"),
        tags$style(type="text/css","#breeder_info table {background-color: #FFE0A3; }", media="screen"),
        tags$style(type="text/css","#breed_table table {background-color: #FFE0A3; }", media="screen")
        )),
      

      conditionalPanel(condition="input.tabs1 == 'Search by Breed'",    #s conditional panelom se v stranski vrstici pokaze widget le ce je izpolnjen pogoj. ta je v tem primeru zavihek, ki je odprt  
                       uiOutput("breedsearch"), #selectImput za izbiro pasme
                       h3(tags$img(src="paw.png", height="20px"), tags$img(src="paw.png", height="30px"), tags$img(src="paw.png", height="20px"),
                       tags$img(src="paw.png", height="30px"), tags$img(src="paw.png", height="20px"), tags$img(src="paw.png", height="30px"),
                       tags$img(src="paw.png", height="20px"), tags$img(src="paw.png", height="30px"), tags$img(src="paw.png", height="20px"),
                       align = 'center')),
      
      #####ZAVIHEK SEARCH BY BREED#######
      conditionalPanel(condition="input.tabs1 == 'Search Breeder'",    
                       uiOutput("search_by_country"), #selectInput za iskalnik drzav 
                       uiOutput("search_by_breed"), #selectInput za iskalnik pasem
                       fluidRow(column(3,offset = 1,uiOutput("map_button"),align="left"), #actionButton za prikaz zemljevida
                                column(3,offset = 4,uiOutput("table_button"),align="right")), #actionButton za prikaz tabele
                       tags$style(type='text/css', "#map_button { horizontal-align: center; height: 40px; width: 140px;}"), #oblikovanje map_button
                       tags$style(type='text/css', "#table_button { horizontal-align: right; height: 40px; width: 130px;}"), #oblikovanje table_button
                       #slike tack
                       h3(tags$img(src="paw.png", height="20px"), tags$img(src="paw.png", height="30px"), tags$img(src="paw.png", height="20px"),
                       tags$img(src="paw.png", height="30px"), tags$img(src="paw.png", height="20px"), tags$img(src="paw.png", height="30px"),
                       tags$img(src="paw.png", height="20px"), tags$img(src="paw.png", height="30px"), tags$img(src="paw.png", height="20px"), align = 'center')
                       ),

      #########################NAJ PASMA##################
                       
      conditionalPanel(condition="input.tabs1 == 'Find your match'",

                       uiOutput("breed_group2"),
                       uiOutput("breed_color"),
                       uiOutput("breed_size"),
                       uiOutput("price"),
                       uiOutput("good_with_kids"),
                       uiOutput("cat_friendly"),
                       uiOutput("dog_friendly"),
                       uiOutput("checkbox"),
                       br(),
                       fluidRow(column(6,offset = 3,uiOutput("button"),align="center")),
                       tags$style(type='text/css', "#button { horizontal-align: center; height: 40px; width: 150px;}")),


      ################### STATS ##################

      conditionalPanel(condition="input.tabs1 == 'All about stats'",
                 
                    h2("Let's review some stats!"),
                    h4("Look up our statistics analysis and find out something new about dogs. "),
                    h3(tags$img(src="paw.png", height="20px"),
                    tags$img(src="paw.png", height="30px"),tags$img(src="paw.png", height="20px"),tags$img(src="paw.png", height="30px"),
                    tags$img(src="paw.png", height="20px"),tags$img(src="paw.png", height="30px"),tags$img(src="paw.png", height="20px"),
                    tags$img(src="paw.png", height="30px"),tags$img(src="paw.png", height="20px"), align = 'center'))
                 
    
    ), 
    
    mainPanel(
      
      tabsetPanel(id = "tabs1", #ime, na katerega se nanasamo pri conditionalPanel

                  
#######################ZAVIHEK PASME########

                  
                  tabPanel("Search by Breed",
                           value = "Search by Breed",
                           h2("Find out more about your favorite breed!"),
                           br(), #prazna vrstica
                           h1(textOutput("chosenbreed")),
                           br(),
                           h3("Basic information"),
                           br(),
                           fluidRow(column(4,align="left",uiOutput("breedphoto", width= "100%")),
                                    column(7, offset=1,align="right", #offset? :)
                                           tableOutput("breedinfo2"))),
                           br(),
                           fluidRow(column(7, 
                                    h3("Overview"),textOutput("overview"),br(),
                                    h3("Body Type"), textOutput("body_type"),br(), 
                                    h3("Coat"),textOutput("coat"), br(),
                                    h3("Color"),textOutput("color_ex"),br(),
                                    h3("Temperament"),textOutput("temperament_ex"),br(),
                                    h3("More Information"),textOutput("more_info")),
                                    column(4,h3("Ratings"),tableOutput("breedinfo1"),align="right")),
                           br(),
                           
                           p("For more information on your chosen dog breed please visit ", uiOutput("wiki"))
                           ),
                  
                  
                  #######ZAVIHEK REJCI########        
                    tabPanel("Search Breeder",
                             value = "Search Breeder",
                             h2("Find a puppy nearby!"),
                             br(),
                             h4(uiOutput("all_breeders_link")),
                             br(),
                             h3(textOutput("table_tittle")),
                             br(),
                             h3(uiOutput("saddog"),align='center'),
                             br(),
                             fluidRow(column(11, align="center",
                                             DT::dataTableOutput("breeder_info"),br(),
                                             h4(textOutput("map_tittle")),
                                             br(),
                                             leafletOutput("map"))),
                             tags$head( tags$style("#breeder_info td,th {border: thin solid gray; }")),
                             tags$head( tags$style("type=text/css", "#all_breeders_link {font-color: black; }"))
                             
                             ),
                                               
                  
                  #######ZAVIHEK NAJ PASMA##########        
                tabPanel("Find your match",
                         value = "Find your match",
                         h2("Find the breed that matches your wishes!"),
                         br(),
                         h3(textOutput("table_tittle2")),
                         br(),
                         fluidRow(column(8, align="center", #align='center' je ne dela???
                                         DT::dataTableOutput("breed_table")),
                                  column(4, align = 'right',
                                         h3(uiOutput("dog_photo")), 
                                         br()))),

                  #########ZAVIHEK STATISTIKA#########
                  tabPanel("All about stats",
                           h3("Average life span"),
                           fluidRow(column(7,align="left", plotOutput("hist_life_span")),
                                    column(5, align="left",h5("Most dogs seem to generally share a common lifespan,
                                      bringing us roughly 10-13 of the best years we???ve ever had. But some breeds make 
                                      the list for having an abnormally long life expectancy. Curious to see which dog breeds has
                                      the longest life span? Check it out here!"),
                                      h4(uiOutput("life_span_link")),
                                      br(),
                                      tableOutput("life_span_text"))),
                           tags$style(type='text/css', "#life_span_text { horizontal-align: center; height: 60px; width: 60px; font-size: 10;}"), #oblikovanje map_button
                           #div(tableOutput("life_span_text"), style = "font-size:80%"),
                           
                           br(),
                           h3("Average puppy price"),
                           fluidRow(column(8,align="center", plotOutput("hist_puppy_price")),
                                    column(2, align="center", p("nek tekst"))),
                           br(),
                           h3("Average dog friendliness"),
                           fluidRow(column(8,align="left", plotOutput("bar_plot")),
                                    column(2, align="center", p("tekst"))), 
                           br(),
                           h3("Breed groups"),
                           fluidRow(column(10,align="left", plotOutput("pie_chart", width = 600, height = 500)),
                                    column(2, align="center", p("nek tekst"))))
                                                             
                         
                  

                 

      )
    )
  )))
